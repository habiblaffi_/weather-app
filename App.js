import React, { useState, useEffect } from 'react';
import { Platform, Text, View, StyleSheet, Image } from 'react-native';
import * as Location from 'expo-location';
import {ActivityIndicator} from "react-native-web";

export default function App() {
    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [longitude, setLongitude] = useState(null)
    const [latitude, setLatitude] = useState(null);
    const [ville, setVille] = useState(null);
    const [temperature, setTemperature] = useState(null);
    const [description, setDescription] = useState(null);
    const [icone, setIcone] = useState(null);
    const [loading, setLoading] = useState(true);

    let key = '28dc43abee98461414c0a95f1abe83c3';

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission non accordée');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            setLocation(location);
        })();
    }, []);

    const MeteoVille = () => {
        fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${location.coords.latitude}&lon=${location.coords.longitude}&appid=${key}&units=metric&lang=fr`)
            .then(function(response){
                return response.json();
            }).then(function(response){
            console.log(response);
            setVille(response.name)
            setLongitude(response.coord.lon)
            setLatitude(response.coord.lat)
            setTemperature(response.main.temp)
            setDescription(response.weather[0].description)
            setIcone("https://openweathermap.org/img/wn/"+response.weather[0].icon+"@2x.png")
            setLoading(false)
        })
    }
    let text = 'Application en cours, veuillez patienter';

    useEffect( () => {
        location && MeteoVille();
    })

    return (
        <View style={styles.container}>
            {loading ? (
                <ActivityIndicator
                    size="small" color="#0000ff"
                    visible={loading}
                    textContent={'Chargement...'}
                /> ) : (
            <>
                <Text style={styles.paragraph}>Longitude : {longitude}</Text>
                <Text style={styles.paragraph}>Latitude : {latitude}</Text>
                <Text style={styles.paragraph}>Ville : {ville}</Text>
                <Text style={styles.paragraph}>Température : {temperature} °C</Text>
                <Text style={styles.paragraph}>{description}</Text>
                <Image
                    style={{ width: 75, height: 75, }}
                    source={{uri:icone}}
                />
            </>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
    },
    paragraph: {
        fontSize: 18,
        textAlign: 'center',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
});